import React, { Component } from 'react';
import './App.sass';

import TodoList from './containers/TodoList/TodoList';
import Item from './components/Item/Item';
import TodoPanel from "./containers/TodoPanel/TodoPanel";
import { connect } from 'react-redux'
import * as actions from './store/actions/todo';


class App extends Component {

  removeItemHandler = (id) => {
    this.props.removeItem(id)
  };
  completedItemHandler = (id) => {
    this.props.completedItem(id)
  };

  componentDidMount() {
    window.addEventListener("beforeunload", () =>
    {
        window.localStorage.setItem('list', JSON.stringify(this.props.list));
    });
    const list = JSON.parse(window.localStorage.getItem('list'));
    if (!list)
      return
    if (list.length !== 0){
      this.props.loadList(list)
    }
  }


  render() {
    let itemsInWork = [];
    let itemsCompleted = [];

    if (this.props.list)
      this.props.list.forEach((item, i) => {
        if(item.completed === false)
          itemsInWork.push(
            <Item key={i}
                  clickedRemove={() => this.removeItemHandler(i)}
                  text={item.text}
                  completed={item.completed}
                  clickedCompleted={() => this.completedItemHandler(i)}/>
          );
        else{
          itemsCompleted.push(
            <Item key={i}
                  clickedRemove={() => this.removeItemHandler(i)}
                  text={item.text}
                  completed={item.completed}
                  clickedCompleted={() => this.completedItemHandler(i)}/>
          )
        }
      });

    return (
      <div className="app">
        <h1 className="app__title">TODO</h1>
        <h4>Roman Sapetin</h4>
        <TodoPanel/>
        <TodoList>
          <h3>In work</h3>
          {itemsInWork.length > 0 ? itemsInWork : "Add a new task"}
          <h3>Completed Task</h3>
          {itemsCompleted}
        </TodoList>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    list: state.list
  }
};

const mapDispatchToProps = dispatch => {
  return {
    removeItem: (newItem) => dispatch(actions.removeItem(newItem)),
    completedItem: (id) => dispatch(actions.completedItem(id)),
    loadList: (list) => dispatch(actions.loadList(list))
  }
};

export default connect(mapStateToProps,mapDispatchToProps)(App);
