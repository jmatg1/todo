import React from 'react'
import PropTypes from 'prop-types'
import './Item.sass'

const item = props => {
  return (
    <div className="Item">
      <div className="Item__remove" onClick={props.clickedRemove}/>
      <div className="Item__completed">
        <input
          onChange={props.clickedCompleted}
          type="checkbox"
          checked={props.completed ? 'checked' : ''}/>
      </div>
      <p>{props.text}</p>
    </div>
  )
}

item.propTypes = {
  text: PropTypes.string.isRequired,
  clickedRemove: PropTypes.func.isRequired,
  clickedCompleted: PropTypes.func.isRequired
}

export default item
