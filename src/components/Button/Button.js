import React from 'react'
import PropTypes from 'prop-types'
import './Button.sass'

const button = props => {
  let styles = []
  styles.push('Button')
  if (props.styled) { styles.push(props.styled) }
  return (
    <button
      className={styles.join(' ')}
      onClick={props.clicked}>{props.children}</button>
  )
}

button.propTypes = {
  clicked: PropTypes.func
}

export default button
