import React from 'react'
import PropTypes from 'prop-types'
import './Input.sass'

const input = props => {
  return (
    <textarea
      onChange={props.changed}
      value={props.value}
      className="Input"
      placeholder={props.placeholder}
    />
  )
}

input.propTypes = {
  changed: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired
}

export default input
