import React from 'react'
import './TodoList.sass'
import PropTypes from 'prop-types'

const TodoList = props => {
  const children = props.children
  return (
    <div className="TodoList">
      {children}
    </div>
  )
}

TodoList.propTypes = {
  childred: PropTypes.element
}

export default TodoList
