import React, {Component} from 'react';
import './TodoPanel.sass';
import Input from "../../components/Input/Input";
import Button from "../../components/Button/Button";
import { connect } from 'react-redux'
import * as actions from '../../store/actions/todo'



class TodoPanel extends Component {
  state = {
    inputValue: ""
  };

  addItemHandler = (event) => {
    event.preventDefault();
    if (this.state.inputValue.length === 0 ) {
      alert("The task cannot be empty");
      return false
    }
    const newItem = {
      text: this.state.inputValue,
      completed: false
    };
    this.props.addItem(newItem)
    this.setState({inputValue: ""})
  };
  inputChangedHandler = (event) => {
    this.setState({inputValue: event.target.value})
  };
  render() {
    return (
      <form onSubmit={this.addItemHandler} className="TodoPanel">
        <Input
          changed={this.inputChangedHandler}
          value={this.state.inputValue}
          placeholder="New task"/>
        <Button styled="mrg-tp">Add Task</Button>
      </form>
    );
  }
}


const mapDispatchToProps = dispatch => {
  return {
    addItem: (newItem) => dispatch(actions.addItem(newItem))
  }
};

export default connect(null,mapDispatchToProps)(TodoPanel);
