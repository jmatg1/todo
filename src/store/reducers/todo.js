import * as actionTypes from '../actions/actionTypes'

const initialState = {
  list: []
}

const todo = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_ITEM:
      const updateListAdd = [...state.list]
      updateListAdd.push(action.newItem)
      return {
        ...state,
        list: updateListAdd
      }
    case actionTypes.REMOVE_ITEM:
      const updateListDel = [...state.list]
      updateListDel.splice(action.id, 1)
      return {
        ...state,
        list: updateListDel

      }
    case actionTypes.LOAD_LIST:
      return {
        ...state,
        list: action.list
      }
    case actionTypes.COMPLETED_ITEM:
      const updateListComp = [...state.list]
      updateListComp[action.id].completed = !updateListComp[action.id].completed
      return {
        ...state,
        list: updateListComp

      }
    default: return state
  }
}

export default todo
