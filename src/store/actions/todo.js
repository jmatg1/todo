import * as actionTypes from './actionTypes'

export const addItem = newItem => {
  return {
    type: actionTypes.ADD_ITEM,
    newItem: newItem
  }
}
export const removeItem = id => {
  return {
    type: actionTypes.REMOVE_ITEM,
    id: id
  }
}
export const loadList = list => {
  return {
    type: actionTypes.LOAD_LIST,
    list: list
  }
}
export const completedItem = id => {
  return {
    type: actionTypes.COMPLETED_ITEM,
    id: id
  }
}
