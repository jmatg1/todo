export const ADD_ITEM = 'ADD_ITEM'
export const REMOVE_ITEM = 'REMOVE_ITEM'
export const LOAD_LIST = 'LOAD_LIST'
export const COMPLETED_ITEM = 'COMPLETED_ITEM'
